#!/usr/bin/groovy
pipeline {

    agent { label 'master' }

    environment {
        APP_NAME = "envoy-gateway-proxy"
        SOURCE_CONTEXT_DIR = ""
        JENKINS_TAG = "latest"
        OCP_API_SERVER = "${OPENSHIFT_API_URL}"
        OCP_TOKEN = readFile('/var/run/secrets/kubernetes.io/serviceaccount/token').trim()
        APPLIER_SKIP_TAGS = "bitbucket-jenkins-webhook"
        APPLIER_TARGET = "app"
        ARTIFACTORY_DEV_REPO = "omnitracs-dev-images.jfrog.io"
        DEV_REPO_KEY = "dev-images"
        ARTIFACTORY_SECRET_NAME = "${CI_CD_NAMESPACE}-artifactory-access-token"
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time: 60, unit: 'MINUTES')
        ansiColor('xterm')
        timestamps()
    }

    stages {

        stage("Apply the Applier!!!") {
            agent { label 'jenkins-slave-ansible' }
            steps {
                script {
                    applyAnsibleInventory("${APPLIER_TARGET}", "${DEV_NAMESPACE}", "${APPLIER_SKIP_TAGS}")
                }
            }
        }

        stage("Build, Bake, and Deploy") {

            agent { label 'jenkins-slave-mvn' }

            stages {

                stage('Bake') {
                    steps {
                        script {
                          openshift.loglevel(5)
		                      timeout(15) { // in minutes
			                       def patchArgs = [namespace: "${env.CI_CD_NAMESPACE}", run_display_url: "${env.RUN_DISPLAY_URL}", build_number: "${env.BUILD_NUMBER}", git_url: "${env.GIT_URL}", git_commit: "${env.GIT_COMMIT}", artifactory_repo: "${env.ARTIFACTORY_DEV_REPO}", repo_key: "${env.DEV_REPO_KEY}", artifactory_folder: "${env.ARTIFACTORY_FOLDER}", app_name: "${env.APP_NAME}"]
			                       //this will allow the git commit image tag to be pushed to Artifactory instead of latest
			                       patchArtifactoryBuildConfigOutputAndLabels(patchArgs)

			                       openshift.withCluster () {

			                          def buildSelector = openshift.startBuild( "${APP_NAME}" )
			                          buildSelector.logs('-f')
			                       }

			                       //since its possible the push failed, we need to check it exists in Artifactory
			                       def checkExistsArgs = [artifactorySecretName: "${ARTIFACTORY_SECRET_NAME}", dockerRepository: "${ARTIFACTORY_FOLDER}/${APP_NAME}", repo: "${DEV_REPO_KEY}", tag: "${env.GIT_COMMIT}"]
	                           if(!artifactoryDockerImageExists(checkExistsArgs)){
	                              error("Image push failed to Artifactory!");
	                           }
                           }
                        }
                    }
                }


                stage('Deploy: Dev') {

                    steps {
                        script {
                            timeout(5) { // in minutes
                                openshift.loglevel(3)
                                openshift.withCluster() {
                                    //Tag the artifactory dev image into the image stream.
                                    openshift.tag("${ARTIFACTORY_DEV_REPO}/${ARTIFACTORY_FOLDER}/${APP_NAME}:${env.GIT_COMMIT} ${DEV_NAMESPACE}/${APP_NAME}:latest")
                                }
                            }
                        }
                    }
                }
                stage("Verify Deployment") {
                    steps {
                        script {
                            timeout(5) { // in minutes
                                verifyDeployment("${APP_NAME}", "${DEV_NAMESPACE}")
                            }
                        }
                    }
                }
                //Smoke Testing checks that no show stopper defect exists in the build which will prevent the testing team to test the application in detail.
                //It should test that events are working, for example.
                stage("Smoke Test") {
                    steps {
                        println "TODO: run some smoke tests!"
                    }
                }
            }
        }
    }
}
